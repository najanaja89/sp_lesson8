﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Net;

namespace sp_lesson8
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void ProcessButtonClick(object sender, RoutedEventArgs e) //Async ставится на метод, который может делать асинхронные задачи
        {
            progressBar.Visibility = Visibility.Visible;
            var name = nameTextBox.Text;
            if (await SaveToDataBase(name)) //Result заставляет главный поток уснуть и дождаться результата
            {
               await SendToNet(name);
                MessageBox.Show("Имя успешно обработано");
            }
            progressBar.Visibility = Visibility.Collapsed;
        }

        private Task<bool> SaveToDataBase(string name)
        {
            //WebClient client = new WebClient();
            //return client.DownloadStringTaskAsync("");

            //return Task.Run(() =>
            //{
            //    Thread.Sleep(5000);
            //    return true;
            //});

            return Task.FromResult(true);
          
        }

        private Task SendToNet(string name)
        {
            return Task.Run(() => Thread.Sleep(3000)); //Task.Run просит созадать новый поток, нужен когда операция не относится к операциям ввода ввывода;
        }
    }
}
